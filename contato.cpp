#include "contato.hpp"


using namespace std;

Contato::Contato()
{
	setNome(NULL);
	setIdade(NULL);
	setTelefone(NULL);
	setEmprego(NULL);
	setCidade(NULL);
}


Contato::Contato(string nome, string telefone)
{
	setNome(nome);
	setTelefone(telefone);
	setIdade(NULL);
	setEmprego(NULL);
	setCidade(NULL);
}

Contato::Contato(string nome, string telefone, string emprego)
{
	setNome(nome);
	setTelefone(telefone);
	setEmprego(emprego);
	setIdade(NULL);
	setCidade(NULL);
}

void Contato::setEmprego(string emprego){
	this->emprego = emprego;
}

void Contato::setCidade(string cidade){
	this->cidade = cidade;
}

string Contato::getEmprego(){
	return emprego;
}

string Contato::getCidade(){
	return cidade;
}
