#ifndef AMIGO_H	
#define AMIGO_H
#include "pessoa.hpp"

class Amigo : public Pessoa{
	private:
		string endereco;
		string aniversario;

	public:
		Amigo();
		Amigo(string nome);
		Amigo(string nome, string idade, string telefone);
		void setEndereco(string endereco);
		void setAniversario(string aniversario);
		string getAniversario();
		string getEndereco();

}

#endif
