#ifndef CONTATO_H
#define CONTATO_H
#include"pessoa.hpp"

class Contato : public Pessoa{
	private:
		string cidade;
		string emprego;

	public:
		Contato();
		Contato(string nome, string telefone);
		Contato(string nome, string telefone, string emprego);
		void setEmprego(string emprego);
		void setCidade(string cidade);
		string getEmprego();
		string getCidade();
	
}

#endif
